package jp.alhinc.sugawara_banri.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		//コマンドライン引数有無のチェック
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		//支店定義ファイルの読み込み
		if(!inputFile(args[0],"branch.lst", "支店", "^[0-9]{3}$", branchNames, branchSales)) {	//パス,ファイル名,エラー時メッセージ,正規表現,定義情報,売上集計
			return;
		}

		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();
		//商品定義ファイルの読み込み
		if(!inputFile(args[0],"commodity.lst", "商品", "^[a-zA-Z0-9]{8}$", commodityNames, commoditySales)) {	//パス,ファイル名,エラー時メッセージ,正規表現,定義情報,売上集計
			return;
		}

		//パスのファイル一覧を取得
		File[] files = new File(args[0]).listFiles();
		List<String> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
			if(files[i].getName().matches("^[0-9]{8}.rcd$") && files[i].isFile()) {
				rcdFiles.add(files[i].getName());
			}
		}
		Collections.sort(rcdFiles);	//リストのソート

		//ファイル連番チェック
		int former;
		int latter;
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			former = Integer.parseInt(rcdFiles.get(i).substring(0,8));
			latter = Integer.parseInt(rcdFiles.get(i + 1).substring(0,8));
			if(latter - former != 1 || i + 1 != former) {
				System.out.println("売上ファイル名が連番になっていません。");
				return;
			}
		}

		//売上ファイルの集計
		String sales = null;
		String line = null;
		String branchCode = null;
		String commodityCode = null;
		Long saleAmount = 0L;
		File file = null;
		FileReader fr = null;
		BufferedReader br = null;
		List<String> rcdList = new ArrayList<>();
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				file = new File(args[0], rcdFiles.get(i));
				fr = new FileReader(file);
				br = new BufferedReader(fr);

				while((line = br.readLine()) != null) {	//売上データの中身を最終行までリストへ格納
					rcdList.add(line);
				}

				//売上ファイルのフォーマットチェック
				if(rcdList.size() != 3) {
					System.out.println(rcdFiles.get(i) + "のフォーマットが不正です。");
					return;
				}

				branchCode = rcdList.get(0);
				commodityCode = rcdList.get(1);
				sales = rcdList.get(2);

				//支店コードの存在チェック
				if(!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFiles.get(i) + "の支店コードが不正です。");
					return;
				}

				//商品コードの存在チェック
				if(!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFiles.get(i) + "の商品コードが不正です。");
					return;
				}

				//売上金額が数字かチェック
				if(!sales.matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}

				//売上金額桁数チェック（支店ごと）
				saleAmount = branchSales.get(branchCode) + Long.parseLong(sales);
				if(saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました。");
					return;
				}

				//売上金額桁数チェック（商品ごと）
				saleAmount = commoditySales.get(commodityCode) + Long.parseLong(sales);
				if(saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました。");
					return;
				}

				branchSales.put(branchCode, branchSales.get(branchCode) + Long.parseLong(sales));	//該当支店に対応する売り上げ金額を格納
				commoditySales.put(commodityCode, commoditySales.get(commodityCode) + Long.parseLong(sales));	//該当商品に対応する売り上げ金額を格納

				rcdList.clear();	//リストの初期化
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました。");
						return;
					}
				}
			}
		}


		//branch.out出力
		if(!salesSumFileOutput(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

		//commodity.out出力
		if(!salesSumFileOutput(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}

	//定義ファイル読み込み処理
	public static boolean inputFile(String path, String fileName, String message, String format, Map<String, String> names, Map<String, Long> sales){
		//定義ファイルの有無チェック
		File file = new File(path, fileName);
		if (!file.exists()){
			System.out.println(message + "定義ファイルが存在しません。");
			return false;
		}

		FileReader fr = null;
		BufferedReader br = null;

		String code = null;
		String name = null;
		String line = null;
		String[] items = null;

		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);

			while((line = br.readLine()) != null) {
				items = line.split(",");
				code = items[0];
				name = items[1];

				//定義ファイルのフォーマットチェック
				if(items.length != 2 || !code.matches(format)) {
					System.out.println(message + "定義ファイルのフォーマットが不正です。");
					return false;
				}

				names.put(code, name);
				sales.put(code, 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}

	//outファイル出力処理
	private static boolean salesSumFileOutput(String path, String fileName, Map<String, String> names, Map<String, Long> sales){
		BufferedWriter bw = null;
		FileWriter fw = null;
		File file = new File(path, fileName);
		try {
			file = new File(path, fileName);
			fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : names.keySet()) {
				bw.write(key);
				bw.write(",");
				bw.write(names.get(key));
				bw.write(",");
				bw.write(sales.get(key).toString());
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}
}


