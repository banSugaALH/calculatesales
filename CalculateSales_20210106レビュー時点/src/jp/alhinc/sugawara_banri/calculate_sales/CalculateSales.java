package jp.alhinc.sugawara_banri.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class CalculateSales {
	public static void main(String[] args) {

//		testRcdCreate(args[0]);	//テストレコード作成用

		try {
			File branchFile = new File(args[0],"branch.lst");

			//支店定義ファイルの有無チェック
			if (!branchFile.exists()){
				System.out.println("支店定義ファイルが存在しません");
				System.exit(0);
			}

			FileReader bfr = new FileReader(branchFile);
			BufferedReader bbr = null;
			bbr = new BufferedReader(bfr);

			String branchLine = null;
			ArrayList <String> branchMst = new ArrayList<>();

			//支店定義ファイルをbranchMstへ保持
			branchLine = bbr.readLine();
			while(branchLine != null){
				branchMst.add(branchLine);
				branchLine = bbr.readLine();
			}

			//出力用の配列にbranchMstから支店情報をコピー
			String[][] branchSalesSum = new String[branchMst.size()][3];
			String[] branchTmp;
			String branchCode = null;
			String branchName = null;
			for(int i=0; i<branchMst.size(); i++) {
				branchTmp = branchMst.get(i).split(",",0);	//カンマごとに区切る
				branchCode = branchTmp[0];//支店コード
				branchName = branchTmp[1];//支店名

				if(branchCode.matches("[0-9]{3}") && !branchName.matches(".*[,].*|.*[\n].*") && branchTmp.length < 3) {//支店コードが数字3桁 かつ カンマまたは改行が含まれていない場合true
					branchSalesSum[i][0] = branchCode;
					branchSalesSum[i][1] = branchName;
					branchSalesSum[i][2] = "0";//売上金額初期値
				}else {
					System.out.println("支店定義ファイルのフォーマットが正しくありません");
					System.exit(0);
				}
			}
			bfr.close();
			bbr.close();


			File commodityFile = new File(args[0],"commodity.lst");

			//商品定義ファイルの有無チェック
			if (!commodityFile.exists()){
				System.out.println("商品定義ファイルが存在しません");
				System.exit(0);
			}

			FileReader cfr = new FileReader(commodityFile);
			BufferedReader cbr = null;
			cbr = new BufferedReader(cfr);

			String commodityLine = null;
			ArrayList <String> commodityMst = new ArrayList<>();

			//支店定義ファイルをcommodityMstへ保持
			commodityLine = cbr.readLine();
			while(commodityLine != null){
				commodityMst.add(commodityLine);
				commodityLine = cbr.readLine();
			}

			//出力用の配列にcommodityMstから支店情報をコピー
			String[][] commoditySalesSum = new String[commodityMst.size()][3];
			String[] commodityTmp;
			String commodityCode = null;
			String commodityName = null;
			for(int i=0; i<commodityMst.size(); i++) {
				commodityTmp = commodityMst.get(i).split(",",0);	//カンマごとに区切る
				commodityCode = commodityTmp[0];//支店コード
				commodityName = commodityTmp[1];//支店名

				if(commodityCode.matches("[A-Z0-9]{8}") && !commodityName.matches(".*[,].*|.*[\n].*") && commodityTmp.length < 3) {//商品コードがアルファベット・数字8桁 かつ カンマまたは改行が含まれていない場合true
					commoditySalesSum[i][0] = commodityCode;
					commoditySalesSum[i][1] = commodityName;
					commoditySalesSum[i][2] = "0";//売上金額初期値

				}else {
					System.out.println("商品定義ファイルのフォーマットが正しくありません");
					System.exit(0);
				}
			}
			cfr.close();
			cbr.close();

			//出力用の配列の最終要素に売上金額を加算
			//rcdファイルの読み込み

			File rcdDir = new File(args[0]);	//コマンドプロンプトで指定したディレクトリ内のファイルをすべて取得
			File rcdFile;
			String rcdSales;
			String mustNotBeLine = null;
			int rcdCnt=1;
			for(String name :rcdDir.list()) {
				if(name.matches("[0-9]{8}.rcd")) {
					if(name.matches(String.format("%08d", rcdCnt)+".rcd")) {
						//ファイルの中身を読み込み
						rcdFile = new File(args[0],name);
						bfr = new FileReader(rcdFile);
						bbr = new BufferedReader(bfr);

						branchCode = bbr.readLine();	//rcdファイルから支店コードの読み込み
						commodityCode = bbr.readLine();	//rcdファイルから商品コードの読み込み
						rcdSales = bbr.readLine();	//rcdファイルから売上金額の読み込み
						mustNotBeLine = bbr.readLine();	//3行でない場合のチェック用変数

						if(mustNotBeLine != null) {
							System.out.println("<" + name + ">のフォーマットが不正です");
							System.exit(0);
						}

						for(int j = 0;j<=branchMst.size();j++) {
							if(j==branchMst.size()) {
								System.out.println("<" + name + ">の支店コードが不正です");
								System.exit(0);
							}else if(branchCode.equals(branchSalesSum[j][0])) {
								branchSalesSum[j][2]=String.valueOf(Long.parseLong(branchSalesSum[j][2]) + Long.parseLong(rcdSales));
								break;
							}
						}
					}else {
						System.out.println("売上ファイル名が連番になっていません");
						System.exit(0);
					}
					rcdCnt++;
				}
			}
			bfr.close();
			bbr.close();
			for(int i = 0;i<branchMst.size();i++) {
				if(Long.parseLong(branchSalesSum[i][2])>9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					System.exit(0);
				}
			}

			//出力用の配列の最終要素に売上金額を加算
			//rcdファイルの読み込み

			File commodityrcdDir = new File(args[0]);	//コマンドプロンプトで指定したディレクトリ内のファイルをすべて取得
			File commodityRcdFile;
			rcdSales = null;
			mustNotBeLine = null;
			rcdCnt=1;
			for(String name :commodityrcdDir.list()) {
				if(name.matches("[0-9]{8}.rcd")) {
					if(name.matches(String.format("%08d", rcdCnt)+".rcd")) {
						//ファイルの中身を読み込み
						commodityRcdFile = new File(args[0],name);
						cfr = new FileReader(commodityRcdFile);
						cbr = new BufferedReader(cfr);

						branchCode = cbr.readLine();	//rcdファイルから支店コードの読み込み
						commodityCode = cbr.readLine();	//rcdファイルから商品コードの読み込み
						rcdSales = cbr.readLine();	//rcdファイルから売上金額の読み込み
						mustNotBeLine = cbr.readLine();	//3行でない場合のチェック用変数

						if(mustNotBeLine != null) {
							System.out.println("<" + name + ">のフォーマットが不正です");
							System.exit(0);
						}


						for(int j = 0;j<=commodityMst.size();j++) {
							if(j==commodityMst.size()) {
								System.out.println("<" + name + ">の商品コードが不正です");
								System.exit(0);
							}else if(commodityCode.equals(commoditySalesSum[j][0])) {
								commoditySalesSum[j][2]=String.valueOf(Long.parseLong(commoditySalesSum[j][2]) + Long.parseLong(rcdSales));
								break;
							}
						}
					}else {
						System.out.println("売上ファイル名が連番になっていません");
						System.exit(0);
					}
					rcdCnt++;
				}
			}
			cfr.close();
			cbr.close();

			for(int i = 0;i<commodityMst.size();i++) {
				if(Long.parseLong(commoditySalesSum[i][2])>9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					System.exit(0);
				}
			}

			//branch.out出力
			FileWriter fw = null;
			String fileName = "branch.out";
			fw = new FileWriter(args[0]+"\\" + fileName);

			//リストの内容を順に処理
			for(int i = 0; i<branchMst.size();i++) {

				fw.append(branchSalesSum[i][0]);
				fw.append(",");
				fw.append(branchSalesSum[i][1]);
				fw.append(",");
				fw.append(branchSalesSum[i][2]);
				fw.append("\r\n");
			}
			fw.flush();
			fw.close();

			//branch.out出力
			fw = null;
			fileName = "commodity.out";
			fw = new FileWriter(args[0]+"\\" + fileName);

			//リストの内容を順に処理
			for(int i = 0; i<commodityMst.size();i++) {

				fw.append(commoditySalesSum[i][0]);
				fw.append(",");
				fw.append(commoditySalesSum[i][1]);
				fw.append(",");
				fw.append(commoditySalesSum[i][2]);
				fw.append("\r\n");
			}
			fw.flush();
			fw.close();
			System.out.println("処理完了");
		}catch(Exception e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}

//	static void testRcdCreate(String filePath) {
//		//テスト用売り上げデータ出力
//		try {
//			FileWriter test = null;
//			String testFileName = null;
//		for(int i = 0; i<11;i++) {
//			testFileName = String.format("%08d", i+1)+".rcd";
//			test = new FileWriter(filePath+"\\" + testFileName);
//
//		//リストの内容を順に処理
//			test.append("001");
//			test.append("\n");
//			test.append("SFT00001");
//			test.append("\n");
//			test.append("10000");
//			test.append("\r\n");
//			test.flush();
//			test.close();
//		}
//
//		}catch(Exception e) {
//
//		}
//	}

}


